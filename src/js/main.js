var children = [];
var aboutMe = ["Hello,", "I'm Dayna.", ""];
var portfolioIsShowing = false;
var contactIsShowing = false;
var aboutIsShowing = false;
var isFinished = true;

/**
 * Document is ready
 */
$(document).ready(function () {

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-89724962-1', 'auto');
        ga('send', 'pageview');

    (function(){
        emailjs.init("user_HkQ3zxlxuovxykKq03rRz");
    })();

    submitForm(function (event) {
        var person = $("#person").val();
        $.cookie('person', person);

        // After user submission, display main page
        $('.loader')
            .delay(1000)
            .queue(function () {
                $('#current-user').text($.cookie('person'));    // insert current user into introduction caption
                $('.loader').css('display', 'none');
                children = [];
                $(".main-nav").children().each(function () {
                    children.push(this);
                })
                fadeInChildElements(children, 500);// remove loading container
                $('.main').css('display', 'block');   // show main container
                $('#social').css('display', 'block');
            });
        var cookieValue = $.cookie("person");
        setContactFormPlaceholder(cookieValue);

    });

    $('#contact-me').submit(function(event){
        emailjs.send("amazon_ses","template_WGQd7ml2",{
            "from_name": $("#contact-name").val(),
            "to_name": "Dayna",
            "reply_to": $("#contact-email").val(),
            "message_html": $("#contact-message").val()})
            .then(function(response) {
                toggleAlert('.success');
                $('#contact-me').trigger('reset');
                    console.log("SUCCESS. status=%d, text=%s", response.status, response.text);
            }, function(err) {
                toggleAlert('.error');
                console.log("FAILED. error=", err);
            });
        return false;
    });


    $("#contact-me").on('click', function(event){
        event.stopPropagation();
    })

    $(".contact-info").on('click', function(event){
        event.stopPropagation();
    })

    $(".about").on('click', function () {
        if (isFinished) {
            isFinished = false;
            console.log("Finished?" + isFinished);
            $(this).removeClass('pulsing'); // remove pulsing hover effects
            if (aboutIsShowing == false) {
                storeCurrentDimensions();
            }
            console.log("Toggling about menu button...");
            onAboutMeToggle('.portfolio', '.contact', '.about');
        }
    });

    $(".portfolio").on('click', function () {
        if (isFinished) {
            isFinished = false;
            $(this).removeClass('pulsing'); // remove pulsing hover effects
            if (portfolioIsShowing == false) {
                storeCurrentDimensions();
            }
            console.log("Toggling portfolio menu button...")
            onPortfolioToggle('.about', '.contact', '.portfolio');
        }
    });

    $(".contact").on('click', function () {
        if (isFinished) {
            isFinished = false;
            $(this).removeClass('pulsing'); // remove pulsing hover effects
            if (contactIsShowing == false) {
                storeCurrentDimensions();
            }
            console.log("Toggling contact menu button...")
            onContactToggle('.about', '.portfolio', '.contact')
        }
    })

    setCurrentWeather();
});

/**
 * Toggle About Me navigation menu button
 * @param first
 * @param second
 * @param classToAnimate
 */
function onAboutMeToggle(first, second, classToAnimate) {
    var aboutWidth = $('.about').data('aboutWidth');
    var aboutHeight = $('.about').data('aboutHeight');
    var mainNavWidth = $('.main-nav').width();
    console.log("Main nav width: " + mainNavWidth);
    var amountToSlide = mainNavWidth - aboutWidth;
    if (aboutIsShowing == false) {
        aboutIsShowing = true;
        $(first).hide('slide', {direction: 'down'}, 1000);
        $(second).hide('slide', {direction: 'down'}, 1000, function () {
            $(classToAnimate).animate({width: '+=' + amountToSlide}, 1000);
            $(classToAnimate).animate({backgroundColor: 'rgba(222, 222, 222, 0.80)'}, 1000);
            $(classToAnimate).animate({height: '+=' + aboutHeight}, 1000, function() {
                easeOutContent('.nav-display', function () {
                    easeInContent('#about-content');
                    isFinished = true;
                })
            });
        });
    } else {
        aboutIsShowing = false;
        easeOutContent('#about-content');
        restoreOriginalText('#about-caption', 'see you around');
        var amountToSlideBack = mainNavWidth - aboutWidth;
        console.log("Amount to slide back: " + amountToSlideBack);
        $(classToAnimate).animate({backgroundColor: '#f5f5f5;'}, 500);
        $(classToAnimate).animate({height: '-=' + aboutHeight}, 1000, function () {
            $(classToAnimate).animate({width: '-='.concat(amountToSlideBack.toString())}, 500, function () {
                $(first).show('slide', {direction: 'up'}, 500);
                $(second).show('slide', {direction: 'up'}, 500);
                restoreOriginalText('#about-caption', 'about');
                $(classToAnimate).addClass('pulsing', function () {
                    easeInContent('.nav-display');
                    isFinished = true;
                });
            });
        });
    }

};

/**
 * Toggle Portfolio menu button
 * @param first
 * @param second
 * @param classToAnimate
 */
function onPortfolioToggle(first, second, classToAnimate) {
    var portfolioWidth = $('.portfolio').data('portfolioWidth');
    var portfolioHeight = $('.portfolio').data('portfolioHeight');
    var mainNavWidth = $('.main-nav').width();
    var amountToSlide = (mainNavWidth - portfolioWidth); // amount to fill main-nav container div

    if (portfolioIsShowing == false) {
        portfolioIsShowing = true;
        $(second).hide('slide', {direction: 'down'}, 1000);
        $(classToAnimate).hide('slide', {direction: 'down'}, 1000, function () {
            $(first).hide('slide', {direction: 'down'}, 1000, function () {
                $(classToAnimate).css("margin-left", 0);
                $(classToAnimate).show('slide', {direction: 'down'}, 1000, function () {
                    $(classToAnimate).animate({width: '+=' + amountToSlide}, 1000, function () {
                        $(classToAnimate).animate({backgroundColor: 'rgba(222, 222, 222, 0.80)'}, 1000);
                        $(classToAnimate).animate({height: '+=' + portfolioHeight}, 1000, function(){
                            easeOutContent('.nav-display', function () {
                                easeInContent('#work-content')
                                isFinished = true;
                            })
                        });    // increase height by 3/2
                    });
                });
            });
        });
    } else {
        portfolioIsShowing = false;
        easeOutContent('#work-content');
        $(classToAnimate).animate({height: '-=' + portfolioHeight}, 500);    // return height back to normal position
        $(classToAnimate).hide('slide', {direction: 'down'}, 500, function () {
            $(classToAnimate).css('margin-left', '4%');
            $(classToAnimate).width(portfolioWidth);
            $(first).show('slide', {direction: 'up'}, 500, function () {
                $(classToAnimate).animate({backgroundColor: '#f5f5f5'}, 500);
                $(classToAnimate).show('slide', {direction: 'up'}, 500, function () {
                    $(second).show('slide', {direction: 'up'}, 500);
                    $(classToAnimate).addClass('pulsing', function(){
                        easeInContent('.nav-display');
                        isFinished = true;
                    });
                });
            });
        });
    }
}

function onContactToggle(first, second, classToAnimate) {
    var contactWidth = $('.contact').data('contactWidth');
    var contactHeight = $('.contact').data('contactHeight');
    var mainNavWidth = $('.main-nav').width();
    var amountToSlide = (mainNavWidth - contactWidth); // amount to fill main-nav container div

    if (contactIsShowing == false) {
        contactIsShowing = true;
        $(classToAnimate).hide('slide', {direction: 'right'}, 1000);
        $(second).hide('slide', {direction: 'down'}, 1000, function () {
            $(first).hide('slide', {direction: 'left'}, 1000, function () {
                $(classToAnimate).css("margin-left", 0);
                $(classToAnimate).show('slide', {direction: 'down'}, 1000, function () {
                    $(classToAnimate).animate({width: '+=' + amountToSlide}, 1000, function () {
                        $(classToAnimate).animate({height: '+=' + contactHeight}, 1000, function () {
                            $(classToAnimate).animate({backgroundColor: 'rgba(222, 222, 222, 0.80)'}, 1000);
                            easeOutContent('.nav-display', function () {
                                easeInContent('#contact-content', function(){
                                    isFinished = true;
                                });
                            });
                        });
                    });
                });
            });
        });
    } else {
        contactIsShowing = false;
        easeOutContent('#contact-content');
        $(classToAnimate).animate({height: '-=' + contactHeight}, 1000, function () {
            $(classToAnimate).hide('slide', {direction: 'down'}, 500, function () {
                $(classToAnimate).css('margin-left', '4%');
                $(classToAnimate).width(contactWidth);
                $(first).show('slide', {direction: 'up'}, 500, function () {
                    $(second).show('slide', {direction: 'up'}, 500, function () {
                        $(classToAnimate).animate({backgroundColor: '#f5f5f5;'}, 500);
                        $(classToAnimate).show('slide', {direction: 'up'}, 500, function () {
                            $(classToAnimate).addClass('pulsing', function () {
                                easeInContent('.nav-display');
                                isFinished = true;
                            });
                        });
                    });
                });
            });
        });
    }
}

/**
 * Scroll words and timeout for about me content
 * @param elemToUpdate
 * @param count
 */
function scrollWordList(elemToUpdate, count, callback) {
    var i = count;
    $(elemToUpdate).text(aboutMe[i]);
    i++;
    if (i < aboutMe.length) {
        setTimeout(function () {
            scrollWordList(elemToUpdate, i);
        }, 2000)
    }

    else if (i == aboutMe.length) {
        setTimeout(function () {
            $('#about-content').show('slide', {direction: 'down'}, 1000);
        }, 1000);
    }

    if (callback) {
        console.log("Ready to go!");
        callback();
    }
}

/**
 * Set the current weather with simpleWeather.js
 */
function setCurrentWeather() {
    console.log("Collecting current weather.");
    $.simpleWeather({
        location: 'Vancouver, BC',
        woeid: '',
        unit: 'C',
        success: function (weather) {
            html = formatWeather(weather.currently.toLowerCase());
            //html = '<p>'+weather.currently.toLowercase() +'</p>';
            $("#weather").html(html);
        },
        error: function (error) {
            console.log(error);
            $("#weather").html("");
        }
    });
}

/**
 * Set original width dimensions of navigation menu buttons
 * (This will be used to animate back to their original widths etc.)
 */
function storeCurrentDimensions() {
    $('.about').data("aboutWidth", $('.about').width());
    $('.about').data("aboutHeight", $('.about').height());
    $('.portfolio').data("portfolioWidth", $('.portfolio').width());
    $('.portfolio').data("portfolioHeight", $('.portfolio').height());
    $('.contact').data("contactWidth", $('.contact').width());
    $('.contact').data("contactHeight", $('.contact').height());
}

function restoreOriginalText(element, text) {
    $(element).animate({'opacity': 0}, 800, function () { // animate caption back to original "about"
        $(this).text(text);
        $(this).css('display', 'block');
    }).animate({'opacity': 1}, 800);
}

function easeOutContent(textElement, callback) {
    $(textElement).animate({'opacity': 0}, 800, function () {
        $(this).css('display', 'none');
        if (callback) {
            callback();
        }
    });
}

function easeInContent(textElement, callback) {
    $(textElement).animate({'opacity': 1}, 200, function () {
        $(this).css('display', 'block');
        if (callback) {
            callback();
        }
    })
}

function hoverOpacity(element, labelOpacity) {
    $(element).mouseover(function () {
        $(labelOpacity).stop(labelOpacity).animate({opacity: 0}, 1500);
    });
    $(element).mouseout(function () {
        $(labelOpacity).stop(labelOpacity).animate({opacity: 1}, 1500);
    });
}

function submitForm(callback) {

    $("#user-input").keypress(function (event) {
        $.removeCookie('person', {path: '/'});
        if (event.which == 13) {
            $("#user-input").submit(function () {
                animateForm(function () {
                    if (callback)
                        callback();
                })
            });

        }
    });
}

function animateForm(callback) {
    easeOutContent('.keyboard-prompt', function () {
        $('.introduce').animate({height: 0}, 2000, function () {
            $('.introduce').css('display', 'none');
            if (callback) {
                callback();
            }
        });
    });
}

function formatWeather(weather) {
    var formatted = weather;
    if (weather == null) {
        return "";
    }
    else if (weather == "showers"){
            return "rainy";
    } else {
        var length = weather.length;
        if (!(weather.charAt(length - 1) == "y") && weather.includes("and")) {
            formatted = weather.concat("y");
            if (weather.includes("and")) {
                var substring = weather.substring(0, weather.indexOf(" and"));
                if (!(substring.charAt(substring.length - 1) == "y")) {
                    var formattedSubstring = substring.concat("y");
                    return formatted.replace(formatted.substring(0, weather.indexOf(" and")), formattedSubstring);
                }
            }
            return formatted;
        }
    }
    return weather;
}

/**
 * Helper method to fade in child navigation elements
 * @param children
 * @param offset
 */
function fadeInChildElements(children, offset) {
    if (children.length > 0) {
        var i = 0;
        for (var childElem; childElem = children.shift();) {
            i++;
            console.log("Current child: " + childElem.class);
            $(childElem).hide().fadeIn(i * offset);
            $(childElem).css('display', 'flex');
        }
    }
}

function setContactFormPlaceholder(name){
    if(name.length > 0)
        $("#contact-name").attr("placeholder", name);
    else
        $("#contact-name").attr("placeholder", "Name");
}

function toggleAlert(alert){
    easeInContent(alert,function(){
        setTimeout(function() {
            easeOutContent(alert, null);
        }, 3000);
    });
}






